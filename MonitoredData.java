import javax.management.monitor.MonitorNotification;

public class MonitoredData {
    String startTime;
    String endTime;
    String activity;

    public MonitoredData(String a, String b , String c){
        startTime=a;
        endTime=b;
        activity=c;
    }

    public String toString(){
        return "Start time is  "+startTime+" End time is  "+endTime+" Activity is "+activity;
    }

}
