import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadingFile {


    String fileName="C:\\Users\\Craciun George\\IdeaProjects\\Java_assignment_5\\src\\Activities.txt";
    List<MonitoredData> dataList=new ArrayList<>();
    List<String> dataString=new ArrayList<>();
    public void reading(){

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            dataString=stream.collect(Collectors.toList());
            for(String x:dataString){

                String[] splited=x.split("\\s+");
                String a=splited[0]+" "+splited[1];
                String a1=splited[2]+" "+splited[3];
                MonitoredData m=new MonitoredData(a,a1,splited[4]);
                dataList.add(m);
                System.out.println(m);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }






    public void count(){


        String count =  dataList.stream()
                .map(l -> (l.startTime.split(" "))[0] + " " + l.endTime.split(" ")[0])
                .collect(Collectors.joining(" "));

        System.out.println("Number of days of monitoring is "+Arrays.stream(count.split(" "))
                .distinct()
                .count());
    }
    public Map<String,Integer> activityAppear(){




        Map<String,Integer> map= dataList.stream()
                    .map(l->l.activity)
                .collect(Collectors.groupingBy(l -> l, Collectors.summingInt(l -> 1)));

        return map;
    }

    public Map<String, Map<String, Integer>> eachActivityforEachDay(){

        return dataList.stream()
                .collect(Collectors.groupingBy(l -> (l.startTime.split(" "))[0], Collectors.groupingBy(l -> l.activity, Collectors.summingInt(l -> 1))));


    }

    public List<String> durationThroughActivities(){

        List<String> myList=dataList.stream()
                .map(l->{

                    try{
                  long mili1=((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(l.endTime).getTime() -(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")) .parse(l.startTime).getTime());
                 return mili1;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return (long)0;


                }).map(b->new SimpleDateFormat("HH:mm:ss").format(new Date(b-7200000)))
                .map(l->"Activity took a time of "+l).collect(Collectors.toList());

           return myList;
    }

    public Map<String,Long> totalDuration(){

        return dataList.stream()
                .collect(Collectors.groupingBy(l -> l.activity, Collectors.summingLong(l -> {

                    try {
                      long  mili1 = ((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(l.endTime).getTime() - (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(l.startTime).getTime());
                        return mili1;

                    } catch (ParseException e) {

                        e.printStackTrace();

                    }

                    return 0;
                })));

    }

}




