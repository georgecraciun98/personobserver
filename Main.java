import java.util.List;
import java.util.Map;

public class Main {


    public static void main(String[] args) throws Exception {
        //reading input and displaying on the screen
        ReadingFile read = new ReadingFile();
        read.reading();
        //count number of days of monitoring
        System.out.println("");
        read.count();
        System.out.println("");

        Map<String,Integer> map=read.activityAppear();
        String[]string= {"Leaving", "Toileting", "Showering",
                "Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
        //display all activities and total count
        for(String m:string){

            System.out.println("Activity is "+m+"  "+map.get(m));
        }
        System.out.println("");

        Map<String, Map<String, Integer>> map1=read.eachActivityforEachDay();
       //activities per day
        map1.forEach((day,nrTimes)->System.out.println("In the day :"+day+" Was executed "+nrTimes));
        //each activity duration through a day
        List<String> finalList=read.durationThroughActivities();
        for(String m:finalList)
            System.out.println(m);
        System.out.println("");

        Map<String,Long> map2=read.totalDuration();
        System.out.println("");

        map2.forEach((a, b) -> System.out.println("Activity is "+a + " Time took is : " +  (b /  3600000) + ":" + ((b / 1000) - (b / 3600000) * 3600) / 60 + ":" + (b/1000 - (b / 3600000) * 3600 - ((b / 1000) - (b / 3600000) * 3600) / 60 * 60)));
    }


}